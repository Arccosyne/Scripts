#!/usr/bin/python

#Generates a list of files that are present (or hash mis-matched) on the source 
#but not on the server side backup
#
#This may take a considerable amount of time depending on the volume of data.
#
#Andrew Damin, 2017

import os

SERVER = "<Destination server IP>"
HOSTNAME = "<Hostname of source machine>"

sources = [
"/etc/bind",
"/home/user"
]

for i in sources:
   command = "rsync -Pahn --checksum %s root@%s:/backup/%s/daily%s" % ( i, SERVER, HOSTNAME, i)
   rsync_out = os.popen(command)
   j = rsync_out.read()
   with open("audit.txt", "w+") as audit:
      audit.write(j)
